const API_URL = {
    CREATE: 'http://localhost/bitrix_api/public/api/deal/save'
};

var app = new Vue({
    el: '#app',
    data() {
        return {
            isSave: false,
            dealID: null,
            contactId: 0,
            userId: 0,
            arrProducts: [],
            dealDetail: {
                ID: 0,
                TITLE: '',
                OPPORTUNITY: 0,
                ASSIGNED_BY_ID: 0,
                TAX_VALUE: 0,
                USERNAME: '',
                COMPANY_NAME: '',
                UF_CRM_1591929672: '',
                UF_CRM_1591843618: '',
            },
            companyDetail: {
                companyID: 0, // Don vi thu huong
                companyName: ''
            }
        }
    },
    created() {
        this.isSave = this.dealID ? true : false;
    },
    methods: {
        handleSearchDeal() {
            let dealID = this.dealID ? this.dealID : null;
            console.log(dealID);
            if (dealID) {
                this.isSave = true;
                this.reloadPage(dealID);
            }
        },
        reloadPage(dealID) {
            this.getDealDetail(dealID);
            this.getProductRowsDeal(dealID);
        },
        replaceAllCharacterString(value) {
            return value.toString().replace(/,/g, '');
        },
        displayFormatCurrency(value) {
            const splitStr = ',';
            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, splitStr);
        },
        getDealDetail(id) {
            BX24.callMethod(
                "crm.deal.get", {
                    id: id
                },
                (result) => {
                    if (result.error())
                        return false;
                    else {
                        console.log(result.data());
                        let data = result.data();
                        this.dealDetail.ID = data.ID;
                        this.dealDetail.TITLE = data.TITLE;
                        this.dealDetail.ASSIGNED_BY_ID = data.ASSIGNED_BY_ID;
                        this.dealDetail.CURRENCY = data.CURRENCY_ID;
                        this.dealDetail.OPPORTUNITY = data.OPPORTUNITY;
                        this.dealDetail.TAX_VALUE = data.TAX_VALUE;
                        this.dealDetail.UF_CRM_1591929672 = data.UF_CRM_1591929672;
                        this.dealDetail.UF_CRM_1591843618 = data.UF_CRM_1591843618;
                        this.contactId = data.CONTACT_ID;
                        this.companyDetail.companyID = data.UF_CRM_1591929672;
                        this.getContact(data.CONTACT_ID);
                        this.getCompany(data.UF_CRM_1591929672);
                        this.getDealCompany(data.COMPANY_ID);
                    }
                }
            );
        },
        getProductRowsDeal(dealID) {
            BX24.callMethod(
                "crm.deal.productrows.get", {
                    id: dealID
                },
                (result) => {
                    if (result.error())
                        console.error(result.error());
                    else {
                        let data = result.data();
                        let filteredArr = data.reduce((acc, current) => {
                            const x = acc.find(item => item.ID === current.ID);
                            if (!x) {
                                return acc.concat([current]);
                            } else {
                                return acc;
                            }
                        }, []);
                        console.log(filteredArr);
                        this.arrProducts = filteredArr;
                    }
                }
            );
        },
        getContact(contactId) {
            BX24.callMethod(
                "crm.contact.get", {
                    id: contactId
                },
                (result) => {
                    if (result.error())
                        console.error(result.error());
                    else {
                        this.dealDetail.USERNAME = result.data().LAST_NAME + ' ' + result.data().NAME;
                    }
                }
            );
        },
        getCompany(companyId) {
            BX24.callMethod(
                "crm.company.get", { id: companyId },
                (result) => {
                    if (result.error())
                        this.companyDetail.companyName = '';
                    else
                        this.companyDetail.companyName = result.data().TITLE;
                }
            );
        },
        getDealCompany(companyId) {
            BX24.callMethod(
                "crm.company.get", { id: companyId },
                (result) => {
                    if (result.error())
                        this.dealDetail.COMPANY_NAME = '';
                    else
                        this.dealDetail.COMPANY_NAME = result.data().TITLE;
                }
            );
        },
        saveDeal() {
            if (!this.isSave) {
                return false;
            }
            let data = {
                products: this.arrProducts,
                dealDetail: this.dealDetail
            };
            axios.post(API_URL.CREATE, data)
                .then(response => {
                    return alert('OKE');
                });
        },
        formatCurrency(value) {
            let realValue = value.toString().replace(/,/g, '');
            const splitStr = ',';
            if (realValue === null || realValue === undefined) {
                return '';
            }
            return realValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, splitStr);
        }
    }
});